# ProctoChat

## ProctoChat is a MERN stack based Chat Application that uses socket.io.

This app has a Login page, a Register page, it has also an avatar system. You can chat with multiple people... (Work In Progress)

## How to install the App

1. Clone this project
2. Install [NodeJS](https://nodejs.org/)
3. In the public directory, do `npm install`
4. Do `npm install` also in the server directory
5. Create a `.env` file and put inside `MONGO_URL = ` and put your own Mongo URI.
6. Then run the app using `npm start` in the server directory and public directory.